import React from 'react';
import './css/titulo.css';

const Titulo = (props) => <h1 className="titulo">{props.texto}</h1>;
export default Titulo;